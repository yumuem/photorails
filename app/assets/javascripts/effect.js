//<script type="text/javascript">
const EFFECT_STEP = 20;

function blur() {
	//window.confirm('demo');
	Pixastic.process(document.getElementById("demoimage"), "blur");
}

function edges() {
	//window.confirm('demo');
	Pixastic.process(document.getElementById("demoimage"), "edges", {mono:false,invert:false});
}

function invert() {
	//window.confirm('demo');
	Pixastic.process(document.getElementById("demoimage"), "invert");
}

function noise(param1) {
	//window.confirm('demo');
	setparam = param1/EFFECT_STEP;
	Pixastic.process(document.getElementById("demoimage"), "noise", {mono:true,amount:setparam,strength:setparam});
}

function pointillize(param1) {
	//window.confirm('demo');
	Pixastic.process(document.getElementById("demoimage"), "pointillize", {radius:param1, density:1.5, noise:1.0, transparent:false});
}

function emboss(param1) {
	//window.confirm('demo');
	Pixastic.process(document.getElementById("demoimage"), "emboss", {strength:param1,direction:"topright"});
}

function solarize() {
	//window.confirm('demo');
	Pixastic.process(document.getElementById("demoimage"), "solarize");
}

function mosaic(param1) {
	//window.confirm('demo');
	Pixastic.process(document.getElementById("demoimage"), "mosaic", {blockSize:param1});
}


function shuffle(list) {
  var i = list.length;
  while (--i) {
    var j = Math.floor(Math.random() * (i + 1));
    if (i == j) continue;
    var k = list[i];
    list[i] = list[j];
    list[j] = k;
  }
  return list;
}

function recon_hor(param1) {
	//window.confirm('recon_hor');
	var base_width = img.naturalWidth;
	var base_height = img.naturalHeight;
//	var used_sleeve_min = 2;
//	var used_sleeve_max = 5;
//	var used_fluc_min = 5;
//	var used_fluc_max = 15;
	var sleeve_num = param1;
	var sleeve_base_height = base_height / sleeve_num;
	var sleeve_base_num = sleeve_num;
	var sleeve_fluc_rate = new Array();
	    sleeve_fluc_rate[sleeve_num-1] = 0;
	var sleeve_height= new Array();
	    sleeve_height[sleeve_num-1]=0;
	var shuffle_seed = new Array();
	for (var i = 0; i < sleeve_num ; i++) {
		shuffle_seed[i] = i;
	};
	shuffle(shuffle_seed);


	for (var i = 0; i < sleeve_fluc_rate.length; i++) {
//		sleeve_fluc_rate[i] = 0.1*(used_fluc_min + Math.random()*(used_fluc_max - used_fluc_min));
//		sleeve_height[i] = sleeve_base_height * sleeve_fluc_rate[i];
		sleeve_height[i] = sleeve_base_height;
	};

	var sleeve_height_put = new Array();
	sleeve_height_put[sleeve_num-1]=0;
    var r;
    var m;
    for (var i = 0; i < sleeve_fluc_rate.length; i++) {
        r = Math.random()*sleeve_num;
        m = sleeve_height[i];
//        sleeve_height_put[i] = sleeve_height[r];
        sleeve_height_put[i] = sleeve_base_height;
        //sleeve_height[r] = m;
    };

    var h;
    var k;
	for(var i = 0; i < sleeve_fluc_rate.length; i++){
		h = shuffle_seed[i] * sleeve_base_height;
		k = i * sleeve_base_height;
		context.drawImage(img,0,h,base_width,sleeve_height[i],0,k,base_width,sleeve_height_put[i]);
//		h = h +sleeve_height[i];
//		k = k +sleeve_height_put[i];
	};
}

function recon_ver(param1) {
	var sleeve_num = param1;
	var base_height = img.naturalHeight ;
	var sleeve_base_width = img.naturalWidth / sleeve_num;
	var sleeve_fluc_rate = new Array();
		sleeve_fluc_rate[sleeve_num-1] = 0;
	var sleeve_width= new Array();
	    sleeve_width[sleeve_num-1]=0;
	var shuffle_seed = new Array();
	for (var i = 0; i < sleeve_num ; i++) {
		shuffle_seed[i] = i;
	};
	shuffle(shuffle_seed);
	for (var i = 0; i < sleeve_fluc_rate.length; i++) {
		sleeve_width[i] = sleeve_base_width;
	};

	var sleeve_width_put = new Array();
	sleeve_width_put[sleeve_num-1]=0;
    for (var i = 0; i < sleeve_fluc_rate.length; i++) {
        sleeve_width_put[i] = sleeve_base_width;
    };

    var h;
    var k;
	for(var i = 0; i < sleeve_fluc_rate.length; i++){
		h = shuffle_seed[i] * sleeve_base_width;
		k = i * sleeve_base_width;
		context.drawImage(img,h,0,sleeve_width[i],base_height,k,0,sleeve_width_put[i],base_height);
	};
}

function recon_sqr(param1) {
	var sleeve_num = param1;
	var base_height = img.naturalHeight ;
	var sleeve_base_width = img.naturalWidth / sleeve_num;
	var sleeve_base_height = img.naturalHeight / sleeve_num;
	var sleeve_fluc_rate = new Array();
		sleeve_fluc_rate[sleeve_num-1] = 0;
	var sleeve_width= new Array();
	    sleeve_width[sleeve_num-1]=0;
	var sleeve_height= new Array();
	    sleeve_height[sleeve_num-1]=0;
	var shuffle_seed = new Array();
	var shuffle_seed2 = new Array();
	for (var i = 0; i < sleeve_num ; i++) {
		shuffle_seed[i] = i;
		shuffle_seed2[i] = i;
	};
	shuffle(shuffle_seed);
	shuffle(shuffle_seed2);

	for (var i = 0; i < sleeve_fluc_rate.length; i++) {
		sleeve_width[i] = sleeve_base_width;
		sleeve_height[i] = sleeve_base_height;
	};

	var sleeve_width_put = new Array();
	sleeve_width_put[sleeve_num-1]=0;
	var sleeve_height_put = new Array();
	sleeve_height_put[sleeve_num-1]=0;
    for (var i = 0; i < sleeve_fluc_rate.length; i++) {
        sleeve_width_put[i] = sleeve_base_width;
        sleeve_height_put[i] = sleeve_base_height;
    };

    var h = 0;
    var k = 0;
    var l = 0;
    var m = 0;
	for(var i = 0; i < sleeve_fluc_rate.length; i++){
		h = shuffle_seed[i] * sleeve_base_width;
		k = i * sleeve_base_width;
		for (var j = 0; j < sleeve_fluc_rate.length;j++) {
			l = shuffle_seed2[j] * sleeve_base_height;
			m = j * sleeve_base_height;
			context.drawImage(img,h,l,sleeve_width[j],sleeve_height[i],k,m,sleeve_width_put[j],sleeve_height_put[i]);
		};
	};
}


function resetDemo() {
	//window.confirm('resetDemo');
	Pixastic.revert(document.getElementById("demoimage"));
}

//</script>

