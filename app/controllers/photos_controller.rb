# coding: utf-8
require 'kconv'

class PhotosController < ApplicationController

  # GET /photos
  # GET /photos.json
  def index
    @photos = Photo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @photos }
    end
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
    @photo = Photo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @photo }
    end
  end

  # GET /photos/new
  # GET /photos/new.json
  def new
    @photo = Photo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @photo }
    end
  end

  # GET /photos/1/edit
  def edit
    @photo = Photo.find(params[:id])
  end

  # POST /photos
  # POST /photos.json
  def create
    if params[:photodata]
        h = Hash.new
        h[:img] = params[:photodata]
        h[:title] = params[:title]
        h[:received] = params[:received]
        h[:shooting] = params[:shooting]
        h[:effect] = params[:effect]
        h[:effectparam1] = params[:effectparam1]
        h[:effectparam2] = params[:effectparam2]
        h[:music] = params[:music]
        params[:photo] = h
    end

    @photo = Photo.new(params[:photo])

    respond_to do |format|
      if @photo.save
        format.html { redirect_to @photo, notice: 'Photo was successfully created.' }
        format.json { render json: @photo, status: :created, location: @photo }
      else
        format.html { render action: "new" }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /photos/1
  # PUT /photos/1.json
  def update
    @photo = Photo.find(params[:id])

    respond_to do |format|
      if @photo.update_attributes(params[:photo])
        format.html { redirect_to @photo, notice: 'Photo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy

    respond_to do |format|
      format.html { redirect_to photos_url }
      format.json { head :no_content }
    end
  end

  # GET /photos/latest
  # GET /photos/latest.json
  def latest
    @photo = Photo.last
    @photos = Photo.all

    update_count() 

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @latest }
    end 
  end

  def update_canvas
    @photo = Photo.last
    @photos = Photo.all

    update_count() 

    respond_to do |format|
      format.html { render :partial => "canvas" }
    end
  end

  def photo_count
    update = false
    pc = PhotoCount.last
    if( pc.count != Photo.all.count)
      update = true
    end

    out = {'canvas_updated' => update ? 1 : 0 }

    respond_to do |format|
      format.json { render json: out }
    end    
  end

  def update_count
    pc = PhotoCount.last
    if( pc==nil )
      pc = PhotoCount.new
    end
    pc.count = Photo.all.size
    pc.save
  end
end
