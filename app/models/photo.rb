class Photo < ActiveRecord::Base
  attr_accessible :received, :shooting, :title
  attr_accessible :picture, :img
  attr_accessible :effect, :effectparam1, :effectparam2, :music
  #has_attached_file :picture, :styles => { :medium => "300x300>", :thumb => "100x100>"} 
  mount_uploader :img, ImgUploader
end
