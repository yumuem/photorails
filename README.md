## About
Show photo effects automatically.
Photos are sent from devices.

## How to upload your photos
 * Please access :
    http://localhost:3000/photos
    http://photorails-yumuem.sqale.jp/photos
 * you need these POST parameters:
    title
    photodata
    received (if you could)
    shooting (if you could)

## TODO
 * Add support for effects.

## Contribution
* Yumi Uematsu
* Takaaki Uematsu
