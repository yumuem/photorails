class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :id
      t.string :setname

      t.timestamps
    end
  end
end
