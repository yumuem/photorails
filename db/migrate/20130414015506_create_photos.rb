class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.datetime :shooting
      t.datetime :received
      t.string :title
#      t.attachment :picture
      t.string :picture

      t.timestamps
    end
  end
end
