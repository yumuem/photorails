class AddEffectToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :effect, :string
  end
end
