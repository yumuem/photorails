class CreateEffects < ActiveRecord::Migration
  def change
    create_table :effects do |t|
      t.integer :id
      t.string :effectname

      t.timestamps
    end
  end
end
