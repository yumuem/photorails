class CreatePhotoCounts < ActiveRecord::Migration
  def change
    create_table :photo_counts do |t|
      t.integer :count
      
      t.timestamps
    end
  end
end
