class AddMusicToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :music, :boolean
  end
end
